﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Player_Network : NetworkBehaviour {

    public GameObject CharMain;
 
    public override void OnStartLocalPlayer()
    {
        GetComponent<CharMovement>().enabled = true;
        CharMain.SetActive(true);
        GetComponent<CharSecMovement>().enabled = true;
    }
}
