﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


   public class CharMovement : NetworkBehaviour {

    public float Speed;
    public float RotationSpeed;
    public List<GameObject> tailObjects = new List<GameObject>();
    public float z_offset = -0.5f;

    [SerializeField]
    public GameObject TailPrefab;
    public Text ScoreText;
    public Text ScoreText2;

    [SyncVar]
    public int score = 0;

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.black;
    }

    // Use this for initialization
    void Start() {
        tailObjects.Add(gameObject);

        ScoreText = GameObject.Find("Canvas").transform.GetChild(0).GetComponent<Text>();
        ScoreText2 = GameObject.Find("Canvas").transform.GetChild(1).GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        if (isLocalPlayer)
            ScoreText.text = score.ToString();

        else
            ScoreText2.text = score.ToString();

        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
        if (this.isLocalPlayer && Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.up * RotationSpeed * Time.deltaTime);
        }
        if (this.isLocalPlayer && Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.up * -1 * RotationSpeed * Time.deltaTime);
        }

        
    }

    public void AddCharSec()
    {
        CmdUpdateScore();
        Vector3 newTailPos = tailObjects[tailObjects.Count - 1].transform.position;
        newTailPos.z -= z_offset;

        tailObjects.Add(GameObject.Instantiate(TailPrefab, newTailPos, Quaternion.identity) as GameObject);
        //NetworkServer.Spawn(TailPrefab);
    }

    [Command]
    public void CmdUpdateScore()
    {
        score++;
    }

}
