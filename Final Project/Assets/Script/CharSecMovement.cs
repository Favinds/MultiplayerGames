﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



   public class CharSecMovement : NetworkBehaviour {

    public float Speed;
    public Vector3 CharSecTarget;
    public int indx;
    public GameObject tailObjects;
    public CharMovement mainChar;

    // Use this for initialization
    void Start() {

            mainChar = GameObject.FindGameObjectWithTag("CharMain").GetComponent<CharMovement>();
            Speed = mainChar.Speed;
            tailObjects = mainChar.tailObjects[mainChar.tailObjects.Count - 2];
            indx = mainChar.tailObjects.IndexOf(gameObject);
    }

    // Update is called once per frame
    void Update() {
        CharSecTarget = tailObjects.transform.position;
        transform.LookAt(CharSecTarget);
        transform.position = Vector3.Lerp(transform.position, CharSecTarget, Time.deltaTime * Speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CharMain"))
        {
            if (indx > 2)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

    }
   }
