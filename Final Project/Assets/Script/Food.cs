﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;


   public class Food : NetworkBehaviour {

    public GameObject FoodPrefab;
    public GameObject curFood;

   /* public override void OnStartLocalPlayer()
    {
        FoodPrefab.SetActive(true);
        curFood.SetActive(true);
    }
    */

    
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CharMain"))
        {
            other.GetComponent<CharMovement>().AddCharSec();
            Destroy(gameObject);
        }
    }

   }
