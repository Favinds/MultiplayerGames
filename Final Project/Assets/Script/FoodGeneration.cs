﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



   public class FoodGeneration : NetworkBehaviour {
    // Range max munculnya Food
    public float Xsize = 8.8f;
    public float Zsize = 8.8f;

    [SyncVar]
    public GameObject FoodPrefab;
    [SyncVar]
    public Vector3 curPos;

    public GameObject curFood;

    [SyncVar]
    public int foodforEat;



    // Use this for initialization
    bool started = false;

    public void OnServerConnect(NetworkConnection conn)
    {
        started = true;
        print("PLAYER CONNECT");
    }

   
    void AddNewFood()
    {
        RandomPos();
        curFood = GameObject.Instantiate(FoodPrefab, curPos, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(curFood);
    }

    void RandomPos()
    {
        curPos = new Vector3(Random.Range(Xsize * -1, Xsize), 0.25f, Random.Range(Zsize * -1, Zsize));
    }

    float counter = 0f;

    [Server]
    void Update()
    {

        counter += Time.deltaTime;

        if (counter <= 5f)
            return;

        if (curFood == null)
        {
            AddNewFood();
        }


        else
        {
            return;
        }
    }
   }
